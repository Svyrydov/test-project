<?php

require_once( get_template_directory() . '/classes/PromoCustomWalkerNavMenu.php');
require_once( get_template_directory() . '/classes/MainCustomWalkerNavMenu.php');
require_once( get_template_directory() . '/classes/FooterCustomWalkerNavMenu.php');
require_once( get_template_directory() . '/facebook-sdk-v4-5.0.0/src/Facebook/autoload.php');
require_once( get_template_directory() . '/twitter-api-php-master/TwitterAPIExchange.php');
/**
 * @param $public_group str group name
 * @param $limit str group name
 * @param $fields array fields https://developers.facebook.com/docs/graph-api/reference/v2.6/post
 * @return bool/array
 */
function facebook_list($public_group, $limit, $fields){
	$fb = new Facebook\Facebook([
		'app_id' => '1623370024643602',
		'app_secret' => '094418a9c03ad96bbc68326e788ce79c',
		'default_graph_version' => 'v2.5',
	]);
	try {
		// Returns a `Facebook\FacebookResponse` object
		$response = $fb->get('/FIFA/feed?limit=5&fields=full_picture,picture,message,id,created_time,from', '1623370024643602|bbSHr9oWbqgnqzB0zlo7UZ8akvs');
		if(!empty($response)){
				$data = $response->getDecodedBody();
			if(!empty($data['data'])){
				return $data['data'];
			}else{
				return false;
			}

		}
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
}

/**
 * @param $public_group str group name
 * https://developers.facebook.com/docs/graph-api/reference/page
 * @return bool/str
 */
function get_facebook_page_logo($public_group){
	$fb = new Facebook\Facebook([
		'app_id' => '1623370024643602',
		'app_secret' => '094418a9c03ad96bbc68326e788ce79c',
		'default_graph_version' => 'v2.5',
	]);
	try {
		$response = $fb->get('/FIFA?fields=name,about,picture', '1623370024643602|bbSHr9oWbqgnqzB0zlo7UZ8akvs');
		if(!empty($response)){

			$data = $response->getDecodedBody();
			if(!empty($data['picture']['data']['url'])){
				return $data['picture']['data']['url'];
			}else{
				return false;
			}

		}
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
}

/**
 * @param $type str GET/POST
 * @param $name str user public name
 * @param $url str endpoint url
 * @param $settings array https://github.com/J7mbo/twitter-api-php/wiki/Get-a-user's-tweets
 * @param $fields array https://github.com/J7mbo/twitter-api-php/wiki/Get-a-user's-tweets
 * @return bool/array
 */
function twitter_list($settings, $url, $name, $type, $fields){
	$settings = array(
		'oauth_access_token' => "3466759575-pPQMk5p9WNHn2Y2RL5g0dsk9LiEArbiljPI4b6A",
		'oauth_access_token_secret' => "GA5bPOpxepfhZCX53jACAKTFrf3wyeNOxHq7HrfNsVziP",
		'consumer_key' => "gAhhUsNTj3PFlHp4J9BmJtUgh",
		'consumer_secret' => "aQZxbYdj3ybimrvTTq424ulnupzGlsq2LhZmlk3Fza0PMgKdQz"
	);
	$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	$getfield = '?screen_name=SvyrydovAndrey';
	$requestMethod = 'GET';

	$twitter = new TwitterAPIExchange($settings);
	$response = $twitter->setGetfield($getfield)
		->buildOauth($url, $requestMethod)
		->performRequest();
	if(!empty($response)){
		return json_decode($response);
	}else{
		return false;
	}

}

function build_socials_list(){
	$facebook = facebook_list(null, null, null);
	$twitter = twitter_list(null, null, null, null, null);
	$result = array();
	$facebook_page_logo = get_facebook_page_logo(null);
	for($i=0; $i <5; $i++){
		if(!empty($facebook[$i])){

			array_push($result, array(
				'type' => 'f',
				'logo' => $facebook_page_logo,
				'picture' => $facebook[$i]['full_picture'],
				'text' => $facebook[$i]['message'],
				'date' => date('U', strtotime($facebook[$i]['created_time']))
			));
		}
		if(!empty($twitter[$i])){
			array_push($result, array(
				'type' => 't',
				'logo' => $twitter[$i]->user->profile_image_url,
				'text' => $twitter[$i]->text,
				'date' => date('U', strtotime($twitter[$i]->created_at))
			));
		}
	}
	return $result;
}

/**
 * @param $ptime date
 * @return string
 */
function time_elapsed_string($ptime)
{
	$etime = time() - $ptime;

	if ($etime < 1)
	{
		return '0 seconds';
	}

	$a = array( 365 * 24 * 60 * 60  =>  'year',
		30 * 24 * 60 * 60  =>  'month',
		24 * 60 * 60  =>  'day',
		60 * 60  =>  'hour',
		60  =>  'minute',
		1  =>  'second'
	);
	$a_plural = array( 'year'   => 'years',
		'month'  => 'months',
		'day'    => 'days',
		'hour'   => 'hours',
		'minute' => 'minutes',
		'second' => 'seconds'
	);

	foreach ($a as $secs => $str)
	{
		$d = $etime / $secs;
		if ($d >= 1)
		{
			$r = round($d);
			return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
		}
	}
}

if ( ! function_exists( 'goodwin_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function goodwin_setup() {

	load_theme_textdomain( 'goodwin', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
        add_image_size( 'default-items', 350, 350, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'promo' => __( 'Promo Menu',      'goodwin' ),
		'main'  => __( 'Main Menu', 'goodwin' ),
		'footer'=> __( 'Footer Menu', 'goodwin' ),
	) );

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

}
endif; // goodwin_setup
add_action( 'after_setup_theme', 'goodwin_setup' );

/**
 * Enqueue scripts and styles.
 */
function goodwin_scripts() {

		$template_url = get_template_directory_uri();
        // Register Styles
        wp_register_style( 'main-stylesheet', get_stylesheet_uri() );
        wp_register_style( 'bootstrap', $template_url .'/css/bootstrap.css' );

        // Enqueue Styles
		wp_enqueue_style('bootstrap');
        wp_enqueue_style('main-stylesheet');

		// Deregister Scripts
		wp_deregister_script( 'jquery' );

        // Register Scripts
		wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
        wp_register_script( 'bootstrap',  $template_url .'/js/bootstrap.min.js', array('jquery'), false, true );
		wp_register_script( 'main',  $template_url .'/js/jquery.main.js', array('jquery'), false, true );

        // Enqueue Scripts
		wp_enqueue_script('jquery');
        wp_enqueue_script('bootstrap');
        wp_enqueue_script('main');
        if ( is_singular() && comments_open() && get_option('thread_comments')) {
                wp_enqueue_script( 'comment-reply' );
        }

}
add_action( 'wp_enqueue_scripts', 'goodwin_scripts' );

/*Register tag [template-url]*/
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

/* ACF functions */
//theme options tab in appearance
if(function_exists('acf_add_options_sub_page')) {
	acf_add_options_sub_page(array(
		'title' => __('Theme Options', 'goodwin'),
		'parent' => 'themes.php',
	));
}

//acf theme functions placeholders
if(!class_exists('acf') && !is_admin()) {
	function get_field_reference( $field_name, $post_id ) {return '';}
	function get_field_objects( $post_id = false, $options = array() ) {return false;}
	function get_fields( $post_id = false) {return false;}
	function get_field( $field_key, $post_id = false, $format_value = true )  {return false;}
	function get_field_object( $field_key, $post_id = false, $options = array() ) {return false;}
	function the_field( $field_name, $post_id = false ) {}
	function have_rows( $field_name, $post_id = false ) {return false;}
	function the_row() {}
	function reset_rows( $hard_reset = false ) {}
	function has_sub_field( $field_name, $post_id = false ) {return false;}
	function get_sub_field( $field_name ) {return false;}
	function the_sub_field($field_name) {}
	function get_sub_field_object( $child_name ) {return false;}
	function acf_get_child_field_from_parent_field( $child_name, $parent ) {return false;}
	function register_field_group( $array ) {}
	function get_row_layout() {return false;}
	function acf_form_head() {}
	function acf_form( $options = array() ) {}
	function update_field( $field_key, $value, $post_id = false ) {return false;}
	function delete_field( $field_name, $post_id ) {}
	function create_field( $field ) {}
	function reset_the_repeater_field() {}
	function the_repeater_field($field_name, $post_id = false) {return false;}
	function the_flexible_field($field_name, $post_id = false) {return false;}
	function acf_filter_post_id( $post_id ) {return $post_id;}
}

function button($atts) {
	extract(shortcode_atts(array(
		'text' => 'learn more',
		'link' => '#',
		'button' => 0
	), $atts));
	if($button == 1){
		return '<a href="'.$link.'" class="btn">'.$text.'</a>';
	}
	return '<a href="'.$link.'">'.$text.'</a>';

}
add_shortcode('button', 'button');

function create_post_types() {
	/* Events */
	$labels_events = array(
		'name' => __('Events', 'goodwin'),
		'singular_name' => __('Event', 'goodwin'),
		'add_new' => __('Add Event', 'goodwin'),
		'add_new_item' => __('Add Event', 'goodwin'),
		'edit_item' => __('Edit Event', 'goodwin'),
		'new_item' => __('New Event', 'goodwin'),
		'view_item' => __('View Event', 'goodwin'),
		'parent_item_colon' => '',
		'menu_name' => __('Events', 'goodwin'),
	);
	$args_events = array(
		'labels' => $labels_events,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 11,
		'menu_icon'=> 'dashicons-calendar-alt',
		'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'comments', 'page-attributes'),
		'taxonomies' => array(),
		'rewrite' => array('slug' => 'event'),

	);

	register_post_type('event', $args_events);

	if(function_exists('flush_rewrite_rules')){
		flush_rewrite_rules();
	}
}
add_action('init', 'create_post_types');