<?php
/**
 * Theme Custom Walker
 */
class PromoCustomWalkerNavMenu extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		if($depth == 0){
			$output .= "\n$indent<div class=\"drop \"><div class=\"drop-holder\"><div class=\"left-col\">\n";
		}else{
			$output .= "\n$indent<ul class=\"sub-menu\">\n";
		}

	}

	function end_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		if($depth == 0){
			$output .= "$indent</div><div class=\"right-col\">
                                                    <ul class=\"tools-list\">
                                                        <li><a href=\"#\"><span class=\"icon-phone\"></span> Enquire or register</a></li>
			<li><a href=\"#\"><span class=\"icon-ask\"></span> FAQ</a></li>
                                                        <li><a href=\"#\"><span class=\"icon-heart\"></span> Wellness Centre</a></li>
                                                        <li><a href=\"#\"><span class=\"icon-goodwin\"></span> Why choose Goodwin?</a></li>
                                                    </ul>
                                                </div></div></div>\n";
		}else{
			$output .= "$indent</ul>\n";
		}

	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';


		if($depth == 1){
			//$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$output .= $indent = '<div class="column '.$depth.'"">';
		}else{
			//$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$output .= $indent = '<li class="'.$depth.'"">';
		}

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        
        $item_output = $args->before;

		if($depth == 0){
			$item_output .= '<a'. $attributes . $class_names.'><span>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        	$item_output .= '</span></a><span class="icon-circle-close"></span>';
		}elseif($depth == 1){
			$item_output .= '<span class="title"><a'. $attributes . $class_names.'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= '</a></span>';
		}else{
			$item_output .= '<a'. $attributes . $class_names.'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= '</a>';
		}

        $item_output .= $args->after;
        
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el(&$output, $item, $depth = 0, $args = array()) {
		if($depth == 1){
			$output .= "</div>\n";
		}else{
			$output .= "</li>\n";
		}
	}
}
