<?php /* Template Name: Home Template */
get_header();
    $slideshow = get_field('slideshow');
    if(!empty($slideshow)):?>
        <div class="home-carousel">
            <?php foreach($slideshow as $slide):?>
                <div class="slide">
                    <?php ?>
                        <div class="bg-stretch">
                            <img src="<?php echo $slide['slide_image'];?>" alt="">
                        </div>
                    <?php ?>
                    <?php ?>
                        <div class="container">
                            <section class="text-block">
                                <?php echo $slide['slide_content'];?>
                            </section>
                        </div>
                    <?php ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif;?>
<?php $lifestyle_blocks = get_field('lifestyle_blocks');
if(!empty($lifestyle_blocks)):?>
    <div class="gallery-block">
        <div class="container">
            <?php $section_title_lifestyle = get_field('section_title_lifestyle');
            if(!empty($section_title_lifestyle)):?>
                <div class="row">
                    <div class="col-md-8">
                        <div class="heading">
                            <div class="h1"><?php echo $section_title_lifestyle;?></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <?php foreach($lifestyle_blocks as $lifestyle_block):?>
                <div class="col-sm-4">
                    <div class="item">
                        <?php if(!empty($lifestyle_block['image'])):?>
                            <div class="image-block">
                                <div class="image-holder">
                                    <img src="<?php echo $lifestyle_block['image']; ?>" alt="">
                                </div>
                            </div>
                        <?php endif;?>
                        <div class="holder">
                            <?php echo $lifestyle_block['content'];?>
                            <?php if(!empty($lifestyle_block['bottom_links'])): ?>
                                <div class="bottom-block">
                                    <ul>
                                        <?php foreach($lifestyle_block['bottom_links'] as $link):
                                            echo '<li>'.apply_filters('the_content', $link['link']).'</li>';
                                        endforeach;?>
                                    </ul>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>

        </div>
    </div>
<?php endif;?>
<?php $section_title_events = get_field('section_title_events');
$promo_events = get_field('promo_events');
$events_count = get_field('events_count');
$events_page_link = get_field('events_page_link');
$events_page_link_text = get_field('events_page_link_text');
if(empty($events_count)){
    $events_count = 3;
}
$args = array(
    'posts_per_page' => $events_count,
    'post_type' => 'event',
    'post_status' => 'publish',
    'meta_key'=>'event_date',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'value'         => date('Ymd',strtotime("today")),
            'compare'       => '>=',
            'type'          => 'DATE'
        )
    )
);

$query_events = new WP_Query( $args );
if($query_events->have_posts() || !empty($promo_events)):
?>
    <div class="gray">
    <div class="container">
        <?php if(!empty($section_title_events)):?>
            <div class="row no-padding">
                <div class="col-md-4 col-sm-6">
                    <div class="heading">
                        <div class="h1"><?php echo $section_title_events;?></div>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <div class="row no-padding">
            <?php if( $promo_events ): ?>
            <div class="col-md-<?php if($query_events->have_posts()){  echo '8'; }else{  echo '12'; }?> col-sm-6">
                <div class="posts-block">
                    <?php foreach( $promo_events as $post): ?>
                        <?php setup_postdata($post); ?>
                        <div class="post-item">
                            <?php if(has_post_thumbnail()):?>
                            <div class="image-block">
                                <?php the_post_thumbnail('full');?>
                            </div>
                            <?php endif;?>
                            <div class="text-block">
                                <div class="head">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <?php $event_date = get_field('event_date');
                                    $date = date_create_from_format('Ymd', $event_date); ?>
                                    <time datetime="<?php echo date_format($date, 'Y-m-d');?>"><?php echo date_format($date, 'j F Y')?></time>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="more"><?php _e('Read more', 'goodwin');?> <span class="icon-arrow-right"></span></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif;
            if ( $query_events->have_posts() ) :?>
            <div class="col-md-<?php if(!empty($promo_events)){  echo '4'; }else{  echo '12'; }?> col-sm-6">
                <div class="calendar-block">
                    <?php while ( $query_events->have_posts()):  $query_events->the_post();?>
                        <div class="date-block">
                            <?php $event_date = get_field('event_date');
                            $date = date_create_from_format('Ymd', $event_date); ?>
                            <div class="date">
                                <strong><?php echo date_format($date, 'j');?></strong>
                                <?php echo date_format($date, 'F Y');?>
                            </div>
                            <div class="event">
                                <div class="title"><?php the_title();?></div>
                                <a href="<?php the_permalink();?>" class="more"><?php _e('View event', 'goodwin');?> <span class="icon-arrow-right"></span></a>
                            </div>
                        </div>
                    <?php endwhile;?>
                    <?php if(!empty($events_page_link)):?>
                        <a href="<?php echo $events_page_link;?>" class="more all"><?php echo $events_page_link_text;?> <span class="icon-arrow-right"></span></a>
                    <?php endif?>
                </div>
            </div>
            <?php wp_reset_postdata();
            endif?>
        </div>
    </div>
</div>
<?php endif?>
<?php $section_title_perks = get_field('section_title_perks');
$content_perks = get_field('content_perks');
$link_perks = get_field('link_perks');
$gallery_perks = get_field('gallery_perks');
if($section_title_perks || $content_perks || $link_perks || $gallery_perks):?>
    <div class="content hidden-xs">
        <div class="container">
            <?php if(!empty($section_title_perks)):?>
            <div class="row no-padding">
                <div class="col-md-4 col-sm-6">
                    <div class="heading">
                        <div class="h1"><?php echo $section_title_perks;?></div>
                    </div>
                </div>
            </div>
            <?php endif;?>
            <div class="cta-block">
                <?php if(!empty($content_perks)):?>
                <div class="text-block">
                    <?php echo $content_perks;?>
                </div>
                <?php endif;?>
                <?php echo apply_filters('the_content', $link_perks);?>
                <?php if(!empty($gallery_perks)): ?>
                    <div class="carousel">
                        <?php foreach( $gallery_perks as $slide ): ?>
                            <div class="slide">
                                <img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['alt']; ?>">
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
<?php endif;?>
<?php $socials_feeds = build_socials_list();
if(!empty($socials_feeds)):?>
<div class="container hidden-sm hidden-xs">
    <div class="social-carousel">
        <?php foreach($socials_feeds as $item):?>
            <?php if($item['type'] == 'f'):?>
                <div class="slide">
                    <div class="text-block">
                        <div class="type-post">
                            <a href="#"><span class="icon-facebook2"></span></a>
                        </div>
                        <div class="post-item">
                            <div class="heading">
                                <div class="small-logo">
                                    <a href="#"><img src="<?php echo $item['logo'];?>" alt=""></a>
                                </div>
                                <div class="title"><a href="#">Goodwin</a></div>

                                <div class="time"><?php echo date('F j', $item['date']);?> at <?php echo date('h:ia', $item['date']);?></div>
                            </div>
                            <p><?php echo $item['text'];?></p>
                        </div>
                        <aside>
                            <ul>
                                <li><a href="#"><span class="icon-thumbs-o-up"></span></a></li>
                                <li><a href="#"><span class="icon-bubble"></span></a></li>
                                <li><a href="#"><span class="icon-undo2"></span></a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="image-block">
                        <img src="<?php echo $item['picture'];?>" alt="">
                    </div>
                </div>
            <?php elseif($item['type'] == 't'):?>
                <div class="slide">
                    <div class="text-block">
                        <div class="type-post">
                            <a href="#"><span class="icon-twitter2"></span></a>
                        </div>
                        <div class="post-item">
                            <div class="heading">
                                <div class="small-logo">
                                    <a href="#"><img src="<?php echo $item['logo'];?>" alt=""></a>
                                </div>
                                <div class="title"><a href="#">@goodwincare</a></div>
                                <div class="time"><?php echo time_elapsed_string( $item['date']);?></div>
                            </div>
                            <p><?php echo $item['text'];?></p>
                        </div>
                        <aside>
                            <ul>
                                <li><a href="#"><span class="icon-undo2"></span></a></li>
                                <li><a href="#"><span class="icon-refresh"></span></a></li>
                                <li><a href="#"><span class="icon-star-full"></span></a></li>
                            </ul>
                        </aside>
                    </div>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    </div>
</div>
<?php endif;?>
<?php get_footer(); ?>