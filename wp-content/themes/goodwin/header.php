<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <?php wp_head(); ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>
<body class="home">
<div id="wrapper">
    <div class="w1">
        <header id="header">
            <div class="container">
                <div class="logo">
                    <?php $logo = get_field('logo', 'option');
                    if(!empty($logo)):?>
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <img src="<?php echo $logo;?>" alt="<?php echo get_bloginfo('name');?>">
                        </a>
                    <?php endif; ?>
                </div>
                <a href="#" class="menu-opener">
                    <span class="text-open"><?php _e('Menu', 'goodwin');?></span>
                    <span class="opener">
                        <span class="lt"></span>
                        <span class="lc"></span>
                        <span class="lb"></span>
                    </span>
                </a>
                <div class="slide-nav">
                    <div class="holder">
                        <nav>
                            <?php
                            $args = array(
                                'theme_location' => 'promo',
                                'menu_class' => 'add-nav',
                                'menu_id' => 'add-nav-menu',
                                'depth' => 3,
                                'walker' => new PromoCustomWalkerNavMenu
                            );
                            if (has_nav_menu('promo')) {
                                 wp_nav_menu($args);
                            }
                            $args = array(
                                'theme_location' => 'main',
                                'menu_class' => 'main-nav',
                                'menu_id' => 'main-nav-menu',
                                'depth' => 3,
                                'walker' => new MainCustomWalkerNavMenu
                            );
                            if (has_nav_menu('main')) {
                                wp_nav_menu($args);
                            }
                            ?>
                        </nav>
                        <?php
                        $contacts_header = get_field('contacts_header', 'option');
                        $socials_header = get_field('socials_header', 'option');
                        if(!empty($contacts_header) || !empty($socials_header)):
                        ?>
                            <div class="contact-block">
                                <?php if(!empty($contacts_header) ):?>
                                    <ul>
                                        <?php foreach($contacts_header as $contact):?>
                                            <li><?php echo $contact['contact']?></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                                <?php if(!empty($socials_header)):?>
                                    <ul class="social-networks">
                                        <?php foreach($socials_header as $social):?>
                                            <li><a href="<?php echo $social['link'];?>"><span class="<?php echo $social['class'];?>"></span></a></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </header>
        <div id="main">