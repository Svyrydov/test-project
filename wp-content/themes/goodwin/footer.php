		</div>
		<footer id="footer">
			<div class="container">
				<section class="widget form-container">
					<form action="#">
						<h5>Newsletter</h5>
						<p>Stay up to date with what’s happening and sign up below.</p>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Full Name">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" placeholder="Email Address">
						</div>
						<button type="submit" class="btn gray">Submit</button>
					</form>
				</section>
				<section class="widget nav-container">
					<h5><?php _e('Navigate', 'goodwin');?></h5>
					<nav>
						<?php
						$args = array(
							'theme_location' => 'footer',
							'menu_class' => 'footer-nav',
							'menu_id' => 'footer-nav-menu',
							'depth' => 1,
							'walker' => new FooterCustomWalkerNavMenu
						);
						if (has_nav_menu('footer')) {
							wp_nav_menu($args);
						}
						?>
					</nav>
				</section>

				<?php
				$contacts_footer = get_field('contacts_footer', 'option');
				$socials_footer = get_field('socials_footer', 'option');
				if(!empty($contacts_footer) || !empty($socials_footer)):
					?>
					<section class="widget social-container">
						<h5><?php _e('contact', 'goodwin');?></h5>
						<?php if(!empty($contacts_footer) ):?>
							<ul>
								<?php foreach($contacts_footer as $contact):?>
									<li><?php echo $contact['contact']?></li>
								<?php endforeach;?>
							</ul>
						<?php endif;?>
						<?php if(!empty($socials_footer)):?>
							<ul class="social-networks">
								<?php foreach($socials_footer as $social):?>
									<li><a href="<?php echo $social['link'];?>"><span class="<?php echo $social['class'];?>"></span></a></li>
								<?php endforeach;?>
							</ul>
						<?php endif;?>
					</section>
				<?php endif;?>
				</div>
			<div class="footer-container">
				<div class="container">
					<p class="copyright"><?php printf( __( 'Copyright  © %d Goodwin. All rights reserved.', 'goodwin' ), date('Y')); ?></p>
					<p class="by"><a href="#"><?php printf( __( 'Site by Cool Guys.', 'goodwin' )); ?></a></p>
				</div>
			</div>
		</footer>
		</div>
		</div>
<?php wp_footer(); ?>
</body>
</html>